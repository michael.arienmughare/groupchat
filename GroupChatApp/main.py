import controller.login_controller as controller
import model.model as model
import view.login_page as login_page


# Triggering the entire project
def login_run():
    # start the application
    c = controller.LoginController(model.Model(), login_page.LoginView())
    c.start_login_page()


if __name__ == '__main__':
    login_run()
