import mysql.connector
import socket
from _thread import start_new_thread
import select
import sys
from threading import *


server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
IP_address = '127.0.0.1'
Port = 5000
server.bind((IP_address, Port))
server.listen(100)
list_of_clients = []


class Database:
    def __init__(self):
        self.mydb = mysql.connector.connect(
            host="localhost",
            user="root",
            password="",
            database="chatapp"
        )
        self.mycursor = self.mydb.cursor()

    def query(self, sqlquery):
        self.mycursor.execute(sqlquery)

    def result_fetchall(self):
        return self.mycursor.fetchall()

    def result_fetchone(self):
        return self.mycursor.fetchone()

    def commit(self):
        self.mydb.commit()


class Model:
    def __init__(self):
        self.db = Database()

    def user_profile(self, username):
        sqlquery = f"SELECT `memberFName`, `memberLName`, `memberEmail` FROM `member` " \
                   f"WHERE `memberUsername` = '{username}'"
        self.db.query(sqlquery)
        myresult = self.db.result_fetchall()
        print(myresult)

    def not_Online(self, username):
        sqlquery = f"update member set `memberOnline` = false WHERE `memberUsername` = '{username}'"
        self.db.query(sqlquery)
        self.db.commit()

    def is_Online(self, username):
        sqlquery = f"update member set `memberOnline` = true WHERE `memberUsername` = '{username}'"
        self.db.query(sqlquery)
        self.db.commit()

    def valid_login(self, username_input, password_input):
        sqlquery = f"SELECT IF (" \
                   f"(SELECT SHA1(`memberPasswd`) FROM member WHERE `memberUsername` = '{username_input}') " \
                   f"= SHA1('{password_input}'), 'true', 'false') AS valid;"
        self.db.query(sqlquery)
        myresult = self.db.result_fetchone()
        if myresult[0] == 'true':
            return True
        else:
            return False
