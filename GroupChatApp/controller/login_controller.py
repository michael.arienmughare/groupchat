import tkinter as tk
import tkinter.messagebox
import controller.chat_controller as chat_controller


class LoginController:
    def __init__(self, model, view):
        self.model = model
        self.view = view

    def start_login_page(self):
        self.view.login_setup(self)
        self.view.start_loginpage_mainloop()

    def app_login(self, username, password):
        # check if username is blank
        if username == "":
            self.view.blank_username_detected()

        # check if password is blank
        elif password == "":
            self.view.blank_password_detected()
        else:
            # check username and password in database server
            login_granted = self.model.valid_login(username, password)

            if login_granted:
                # set memberOnline status to True
                self.model.is_Online(username)

                # close login window
                self.view.close_login_window()

                # open chat window
                chat_controller.chat_run(username)
            else:
                # clear passwd and stay on login screen
                self.view.clear_login_passwd_entry()
                tk.messagebox.showinfo("Chat Away", "Wrong username or password combination!")





