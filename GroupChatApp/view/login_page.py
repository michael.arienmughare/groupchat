import tkinter as tk
from tkinter import *
import tkinter.messagebox


class LoginView:
    def login_setup(self, controller):
        # login window
        self.loginWindow = tk.Tk()
        self.inputFrame = tk.Frame(self.loginWindow, bd=2, height=200, width=200, bg='orange')
        self.username_label = tk.Label(self.inputFrame,
                                       text="Username:",
                                       bg='orange')
        self.username_entry = tk.Entry(self.inputFrame)
        self.password_label = tk.Label(self.inputFrame,
                                       text="Password:",
                                       bg='orange')
        self.password_entry = tk.Entry(self.inputFrame)
        self.enter_button = tk.Button(self.inputFrame,
                                      text='ENTER',
                                      command=lambda: controller.app_login(self.username_entry.get(),
                                                                           self.password_entry.get()))

        # set the window title
        self.loginWindow.title('Chat Away')

        # set the window size
        self.loginWindow.geometry('500x600')

        # not resizeable
        self.loginWindow.resizable(False, False)

        # set window background
        self.loginWindow.configure(bg='orange')

        # Create window icon
        self.loginWindow.iconbitmap('chat_2.ico')

        # create input frame
        self.inputFrame.grid(padx=150, pady=200)

        # Create username label and entry
        self.username_label.grid(row=15,
                                 column=3,
                                 columnspan=2,
                                 sticky=W)

        self.username_entry.grid(row=15,
                                 column=5,
                                 columnspan=2)
        self.username_entry.focus()

        # Create password label and entry
        self.password_label.grid(row=16,
                                 column=3,
                                 columnspan=2,
                                 sticky=W)

        self.password_entry.grid(row=16,
                                 column=5,
                                 columnspan=2)

        # Create Enter button
        self.enter_button.grid(row=17,
                               column=5,
                               columnspan=2,
                               padx=10,
                               pady=10)

    def start_loginpage_mainloop(self):
        self.loginWindow.mainloop()

    def clear_login_passwd_entry(self):
        self.password_entry.delete(0, "end")

    def blank_username_detected(self):
        tk.messagebox.showinfo("Chat Away", "Please enter a username")

    def blank_password_detected(self):
        tk.messagebox.showinfo("Chat Away", "Please enter a password")

    def close_login_window(self):
        self.loginWindow.destroy()

