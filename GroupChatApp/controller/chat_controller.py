import main
import view.chat_page as chat
import model.model as model


class ChatController:
    def __init__(self, model, view, username):
        self.model = model
        self.view = view
        self.username = username

    def start_chat_page(self):
        self.view.chat_setup(self, self.username)
        self.view.start_chatpage_mainloop()

    def chat_logout(self):
        self.model.not_Online(self.username)
        self.view.close_chat_window()
        main.login_run()

    def chat_exit(self):
        self.model.not_Online(self.username)
        self.view.close_chat_window()

    def profile(self, username):
        self.model.user_profile(username)

    def click_send(self, username, message):
        self.view.clear_input_message()
        message = username + "> " + message + "\n"
        self.view.display_chat_message(message)


def chat_run(username):
    # start the chat view
    d = ChatController(model.Model(), chat.ChatView(), username)
    d.start_chat_page()
