import tkinter as tk
from tkinter import *
from tkinter.scrolledtext import ScrolledText


class ChatView:
    def chat_setup(self, controller, username):
        self.username = username
        # chat window
        self.chatWindow = tk.Tk()
        self.chatWindow.title('Chat Away')
        self.chatWindow.geometry('600x550')
        self.chatWindow.resizable(False, False)
        self.chatWindow.configure(bg='orange')

        # Create icon for top left corner of canvas
        self.chatWindow.iconbitmap('chat_2.ico')

        # Profile menu
        self.user_menu_button = tk.Menubutton(self.chatWindow,
                                              text=f'{self.username}',
                                              activebackground='orange',
                                              anchor=E,
                                              relief='raised')
        self.user_menu_button.grid(row=0,
                                   column=6,
                                   rowspan=1,
                                   columnspan=2,
                                   padx=80,
                                   pady=10)
        self.user_menu_button.menu = tk.Menu(self.user_menu_button, tearoff=0)
        self.user_menu_button['menu'] = self.user_menu_button.menu
        profile = IntVar()
        logout = IntVar()
        exit_app = IntVar()
        self.user_menu_button.menu.add_checkbutton(label='Profile',
                                                   variable=profile,
                                                   command=lambda: controller.profile(self.username))
        self.user_menu_button.menu.add_checkbutton(label='Logout',
                                                   variable=logout,
                                                   command=lambda: controller.chat_logout())  # set to login
        self.user_menu_button.menu.add_checkbutton(label='Exit',
                                                   variable=exit_app,
                                                   command=lambda: controller.chat_exit())

        # Show who messages are addressed to
        self.msg_addressed_to = tk.Label(self.chatWindow, bd=2, text="@ Everyone", relief='groove', anchor=W)
        self.msg_addressed_to.grid(columnspan=8, column=0, row=1, padx=80, sticky=W + E)  # sticky???

        # Display messages
        self.incoming_msg = ScrolledText(self.chatWindow, width=50, height=20, borderwidth=5, state=tk.DISABLED)
        self.incoming_msg.grid(row=2, column=0, columnspan=8, rowspan=12, padx=80, pady=10)

        # Input text to send out
        self.input_msg = ScrolledText(self.chatWindow, width=35, height=3, borderwidth=5)
        self.input_msg.grid(row=16, column=0, columnspan=7, rowspan=2, padx=80, pady=10, sticky=W + E)

        # Send button
        self.send_button = tk.Button(self.chatWindow, text='SEND', height=3, width=6,
                                     command=lambda: controller.click_send(self.username,
                                                                           self.input_msg.get('1.0', 'end-1c')
                                                                           ))
        self.send_button.grid(row=16, column=6, rowspan=2, columnspan=2, padx=80, pady=10, sticky=W + E)

    def start_chatpage_mainloop(self):
        self.chatWindow.mainloop()

    def close_chat_window(self):
        self.chatWindow.destroy()

    def display_chat_message(self, message):
        self.incoming_msg.configure(state='normal')
        self.incoming_msg.insert(tk.INSERT, message)
        self.incoming_msg.configure(state='disabled')

    def clear_input_message(self):
        self.input_msg.delete(1.0, END)
